#+TITLE: Repositories for Compiler lab 2022

This repository contains (as submodules) all the repositories of
students who are registered for the compilers lab.


* Instructions for students.

  You do not need to clone or do anything with this repo. Your role
  here is limited to watching this repo to see if your commits have be
  updated. You assignment repo will be present as a submodule here and
  all you need to check is whether the hash there matches with what
  you have in your repo.


* Instruction for TA/Tutor

  The main actors who use this repo is the TA's and instructors. You
  will make no changes except for updating the submodules. It is a
  good idea to start making a fork of this repository on gitlab.

1. Make a clone of this repository on gitlab

2. Make a local copy on you machine.

3. At the end of the deadline do the following

   #+BEGIN_SRC

   git submodule update --remote # update all submodule to the latest
   git tag <YYYY-MM-DD>          # tag with the date of submission (deadline).

   #+END_SRC

4. You can the push the updated repo to your gitlab version and send
   me a pull request.

5. Evaluation of individual assignment can be done by going to the
   appropriate repository (do not change any file there as it will
   interact badly with the submodule update).
